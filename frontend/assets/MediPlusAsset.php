<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class MediPlusAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300, 400, 700" rel="stylesheet',
        'css/bootstrap.css',
        'css/animate.css',
        'css/owl.carousel.min.css',
        'css/bootstrap-datepicker.css',
        'css/jquery.timepicker.css',

        'fonts/ionicons/css/ionicons.min.css',
        'fonts/fontawesome/css/font-awesome.min.css',
        'fonts/flaticon/font/flaticon.css',

        'css/style.css',
    ];
    public $js = [
        'js/jquery-3.2.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/bootstrap-datepicker.js',
        'js/jquery.timepicker.min.js',
        'js/jquery.waypoints.min.js',
        'js/main.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
