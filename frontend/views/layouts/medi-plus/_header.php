<?php


use \yii\helpers\Url;

$actionId = Yii::$app->controller->action->id;
?>

<header role="banner">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-5">
                    <ul class="social list-unstyled">
                        <!--<li><a href="#"><span class="fa fa-facebook"></span></a></li>-->
                        <!--<li><a href="#"><span class="fa fa-twitter"></span></a></li>-->
                        <li><a href="https://www.instagram.com/smartmedservice"><span
                                        class="fa fa-instagram"></span></a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-7 text-right">
                    <p class="mb-0">
                        <a href="#" class="cta-btn" data-toggle="modal" data-target="#modalAppointment">Записаться на
                            прием</a></p>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="<?= \yii\helpers\Url::home() ?>">Smart<span>Med</span>Service</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05"
                    aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample05">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle <?= $actionId === 'services' ? 'active' : '' ?>" href="<?=
                        Url::toRoute('services/') ?>" id="dd-services"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Услуги</a>
                        <div class="dropdown-menu" aria-labelledby="dd-services">
                            <a class="dropdown-item" href="/hotels">Отели</a>
                            <a class="dropdown-item" href="/medical-coordinator">Медицинский координатор</a>
                            <a class="dropdown-item" href="/airport">Трансфер с аэропорта и обратно</a>
                            <a class="dropdown-item" href="/telephone">Аренда телефона</a>
                            <a class="dropdown-item" href="/tourism">Туризм</a>
                            <!--<a class="dropdown-item" href="<? /*= Url::toRoute('services/') */ ?>">Services</a>-->
                        </div>

                    </li>
                    <!--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="<? /*= Url::toRoute('doctors/') */ ?>" id="dropdown05"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Врачи</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown05">
                            <a class="dropdown-item" href="<? /*= Url::toRoute('doctors/') */ ?>">Find Doctors</a>
                            <a class="dropdown-item" href="#">Practitioner</a>
                        </div>
                    </li>-->

                    <li class="nav-item">
                        <a class="nav-link <?= $actionId === 'partners' ? 'active' : '' ?>" href="#partners">Наши партнеры</a>
                    </li>

                    <!--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle <?/*= $actionId === 'services' ? 'active' : '' */?>" href="<?/*=
                        Url::toRoute('helth/') */?>" id="dd-helth"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">О здоровье</a>
                        <div class="dropdown-menu" aria-labelledby="dd-helth">
                            <a class="dropdown-item" href="<?/*= Url::toRoute('surgery/')
                            */?>">Пластическая хирургия</a>
                        </div>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link <?/*= $actionId === 'news' ? 'active' : '' */?>" href="<?/*= Url::toRoute('news/')
                        */?>">Новости</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?/*= $actionId === 'reviews' ? 'active' : '' */?>"
                           href="<?/*= Url::toRoute('reviews/')
                           */?>">Отзывы</a>
                    </li>-->

                    <li class="nav-item">
                        <a class="nav-link <?= $actionId === 'contact' ? 'active' : '' ?>" href="<?= Url::toRoute
                        ('contact/') ?>">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>