<?php

?>

<footer class="site-footer" role="contentinfo">
    <div class="container">
        <div class="row mb-5 element-animate">
            <div class="col-md-3 mb-5">
                <h3>Услуги</h3>
                <ul class="footer-link list-unstyled">
                    <li><a href="#">Отели</a></li>
                    <li><a href="#">Медицинский координатор</a></li>
                    <li><a href="#">Трансфер с аэропорта</a></li>
                    <li><a href="#">Аренда телефона</a></li>
                    <li><a href="#">Туризм</a></li>
                </ul>
            </div>
            <!--<div class="col-md-3 mb-5">
                <h3>Новости</h3>
                <ul class="footer-link list-unstyled">
                    <li><a href="#">News &amp; Press Releases</a></li>
                    <li><a href="#">Health Care Professional News</a></li>
                    <li><a href="#">Events &amp; Conferences</a></li>
                </ul>
            </div>-->
            <div class="col-md-3 mb-5">
                <h3>О нас</h3>
                <ul class="footer-link list-unstyled">
                    <li><a href="#">Интимная пластическая хирургия</a></li>
                    <li><a href="#">Эндокринология</a></li>
                    <li><a href="#">Пластическая хирургия</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Отзывы</a></li>
                </ul>
            </div>
            <div class="col-md-3 mb-5">
                <h3>Контакты</h3>
                <p class="mb-5">г. Алматы, ул Сатпаева 999</p>

                <h4 class="text-uppercase mb-3 h6 text-white">Email</h4>
                <p class="mb-5"><a href="mailto:info@yourdomain.com">smartmedkaz@gmail.com</a></p>

                <h4 class="text-uppercase mb-3 h6 text-white">Телефон</h4>
                <p>+7 777 777 77 77</p>

            </div>
            <div class="col-md-3 mb-5">
                <h3>Контакты в Корее</h3>
                <p class="mb-5">Seoul, 110-110, Korea</p>

                <h4 class="text-uppercase mb-3 h6 text-white">Email</h4>
                <p class="mb-5"><a href="mailto:info@yourdomain.com">info@smartmedservice.com</a></p>

                <h4 class="text-uppercase mb-3 h6 text-white">Телефон</h4>
                <p>+82 10 3223 3442</p>

            </div>
        </div>

        <div class="row pt-md-3 element-animate">
            <div class="col-md-12">
                <hr class="border-t">
            </div>
            <div class="col-md-6 col-sm-12 copyright">
                <p>&copy; <?= date('Y')?> SmartMedService</a></p>
            </div>
            <!--<div class="col-md-6 col-sm-12 text-md-right text-sm-left">
                <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
                <a href="#" class="p-2"><span class="fa fa-twitter"></span></a>
                <a href="#" class="p-2"><span class="fa fa-instagram"></span></a>
            </div>-->
        </div>
    </div>
</footer>
