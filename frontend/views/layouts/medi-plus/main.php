<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use common\widgets\Alert;
use frontend\assets\MediPlusAsset;

MediPlusAsset::register($this);
$this->title = 'Smart Med Service'
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300, 400, 700" rel="stylesheet">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('_header')?>
<!-- END header -->


<?= Alert::widget() ?>
<?= $content ?>


<?= $this->render('_cta')?>
<!-- END cta-link -->


<?= $this->render('_footer')?>
<!-- END footer -->


<!-- Modal -->
<?= $this->render('_modal')?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>