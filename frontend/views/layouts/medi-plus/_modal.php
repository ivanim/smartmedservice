<?php

use yii\bootstrap\ActiveForm;

?>

<div class="modal fade" id="modalAppointment" tabindex="-1" role="dialog" aria-labelledby="modalAppointmentLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h5 class="modal-title" id="modalAppointmentLabel">Appointment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>-->
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'enableClientValidation' => false,
                    'action' => '/submit-appointment'
                ]); ?>
                <div class="form-group">
                    <label for="appointment_name" class="text-black">Ваше имя</label>
                    <input type="text" class="form-control" id="appointment_name" name="appointment_name">
                </div>
                <div class="form-group">
                    <label for="appointment_phone" class="text-black">Телефон</label>
                    <input type="text" class="form-control" id="appointment_phone" name="appointment_phone" required>
                </div>
                <div class="form-group">
                    <label for="appointment_email" class="text-black">Email</label>
                    <input type="text" class="form-control" id="appointment_email" name="appointment_email">
                </div>
                <!--<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="appointment_date" class="text-black">Date</label>
                            <input type="text" class="form-control" id="appointment_date">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="appointment_time" class="text-black">Time</label>
                            <input type="text" class="form-control" id="appointment_time">
                        </div>
                    </div>
                </div>-->

                <div class="form-group">
                    <label for="appointment_message" class="text-black">Ваше сообщение</label>
                    <textarea name="appointment_message" id="appointment_message" class="form-control" cols="30" rows="7"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Отправить" class="btn btn-primary">
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>

