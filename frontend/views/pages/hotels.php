<?php

use kv4nt\owlcarousel\OwlCarouselWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отели';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="text-center">
        <h1>
            <?= $this->title ?>
        </h1>
    </div>
</div>

<section class="section stretch-section" style="padding: 7em 0 1em 0">
    <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
            <div class="col-md-8 text-center mb-5">
                <h2 class="text-uppercase heading border-bottom mb-4">Отель Лотте (Lotte Hotel & World)</h2>

                <?php
                    OwlCarouselWidget::begin([
                        'container' => 'div',
                        'containerOptions' => [
                            'id' => 'lotte_slider',
                            'class' => 'lotte_slider'
                        ],
                        'pluginOptions' => [
                            'autoplay' => true,
                            'autoplayTimeout' => 3000,
                            'items' => 1,
                            'loop' => true,
                            'itemsDesktop' => [1199, 3],
                            'itemsDesktopSmall' => [979, 3]
                        ]
                    ]);
                ?>
                <div><img src="/media/hotels/lotte/lotte_01.jpg" alt="Image 1"></div>
                <div><img src="/media/hotels/lotte/lotte_02.jpg" alt="Image 2"></div>
                <div><img src="/media/hotels/lotte/lotte_03.jpg" alt="Image 3"></div>
                <div><img src="/media/hotels/lotte/lotte_04.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/lotte/lotte_05.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/lotte/lotte_06.jpg" alt="Image 4"></div>

                <?php OwlCarouselWidget::end(); ?>

                <div class="mb-0 lead text-justify">
                    <p>5-звездочный отель Lotte расположен рядом с самым большим парком развлечений Lotte World
                        Adventure, недалеко от метро линий 2 и 8. К услугам гостей хорошо оборудованный фитнес-центр,
                        крытый бассейн и 5 ресторанов.</p>
                    <p>Отель Lotte World расположен в центре города, в деловом районе Каннам. Предоставляется бесплатная
                        парковка.</p>
                    <p>В просторных номерах есть телевизор с плоским 37-дюймовым экраном, комфортные кресла, большие
                        окна и ковровое покрытие. В ванных комнатах имеется фен и отличные туалетные принадлежности.</p>
                    <p>Гости могут посетить поле для гольфа, сауну и салон. Персонал отеля предлагает бизнес-услуги и
                        услуги няни.</p>
                    <p>Ресторан Toh Lim сервирует вкусные китайские блюда. В ресторане La Seine предлагается "шведский
                        стол" из разнообразных блюд. Имеется также гастроном и немецкая пивоварня.</p>
                    <p class="text-black">Адрес : 240 Олипик-ро, Сонпха-гу, 138220 Сеул, Южная Корея</p>
                </div>

            </div>


            <div class="col-md-8 text-center mb-5">
                <h2 class="text-uppercase heading border-bottom mb-4">Отель Прима (Prima Hotel)</h2>

                <?php
                    OwlCarouselWidget::begin([
                        'container' => 'div',
                        'containerOptions' => [
                            'id' => 'prima_slider',
                            'class' => 'prima_slider'
                        ],
                        'pluginOptions' => [
                            'autoplay' => true,
                            'autoplayTimeout' => 3000,
                            'items' => 1,
                            'loop' => true,
                            'itemsDesktop' => [1199, 3],
                            'itemsDesktopSmall' => [979, 3]
                        ]
                    ]);
                ?>
                <div><img src="/media/hotels/prima/prima_01.jpg" alt="Image 1"></div>
                <div><img src="/media/hotels/prima/prima_02.jpg" alt="Image 2"></div>
                <div><img src="/media/hotels/prima/prima_03.jpg" alt="Image 3"></div>
                <div><img src="/media/hotels/prima/prima_04.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/prima/prima_05.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/prima/prima_06.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/prima/prima_07.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/prima/prima_08.jpg" alt="Image 4"></div>

                <?php OwlCarouselWidget::end(); ?>

                <div class="mb-0 lead text-justify">
                    <p>Отель Prima расположен всего в 2 минутах ходьбы от станции метро
                        Cheongdam и в 10 минутах езды от района Каросу-Гиль. К услугам гостей просторные
                        общественные купальни с различными гидромассажными ваннами и саунами. На всей территории
                        работает бесплатный Wi-Fi.</p>
                    <p>Отель Prima находится в 5 минутах езды от храма Бонгенса и конгресс-центра COEX, а поездка до
                        авиатерминала Корея-Сити занимает 8 минут. Гости могут за 1 час 40 минут добраться от отеля
                        до аэропорта Инчхона на трансферных автобусах маршрута № 6006.</p>
                    <p>В числе удобств каждого номера — телевизор с плоским экраном и холодильник, а ванная комната
                        с ванной укомплектована бесплатными туалетно-косметическими принадлежностями. В отеле Prima
                        работают ресторан и фитнес-центр. Помимо этого, в распоряжении гостей помещения для
                        совещаний, камера хранения багажа и химчистка. На территории обустроена бесплатная
                        парковка.</p>
                    <p class="text-dark">Адрес : 52-3 Чонгдам-донг, Кангнам-гу, 135-100 Сеул, Южная Корея</p>
                </div>
            </div>

            <div class="col-md-8 text-center mb-5">
                <h2 class="text-uppercase heading border-bottom mb-4">Резиденция Eastern I</h2>

                <?php
                OwlCarouselWidget::begin([
                    'container' => 'div',
                    'containerOptions' => [
                        'id' => 'eastern_slider',
                        'class' => 'eastern_slider'
                    ],
                    'pluginOptions' => [
                        'autoplay' => true,
                        'autoplayTimeout' => 3000,
                        'items' => 1,
                        'loop' => true,
                        'itemsDesktop' => [1199, 3],
                        'itemsDesktopSmall' => [979, 3]
                    ]
                ]);
                ?>
                <div><img src="/media/hotels/eastern/eastern_01.jpg" alt="Image 1"></div>
                <div><img src="/media/hotels/eastern/eastern_02.jpg" alt="Image 2"></div>
                <div><img src="/media/hotels/eastern/eastern_03.jpg" alt="Image 3"></div>
                <div><img src="/media/hotels/eastern/eastern_04.jpg" alt="Image 4"></div>

                <?php OwlCarouselWidget::end(); ?>

                <div class="mb-0 lead text-justify">
                    <p>Отель Eastern I, расположен в финансовом центре Сеула, в районе Кангнам, где расположились офисы
                        крупнейших корейских и мировых компании.</p>
                    <p>Мы постарались предусмотреть всё для Вашего комфортабельного пребывания в столице Южной Кореи. В
                        Вашем распоряжении высокоскоростной интернет/Wi-Fi, стиральная машина, кухонные приборы, которые
                        создадут атмосферу дома, за разумную цену.</p>

                    <p class="text-dark">Адрес : 735-15, Йоксам 1-донг, Кангнам-гу, Сеул, Южная Корея</p>
                </div>
            </div>


            <div class="col-md-8 text-center mb-5">
                <h2 class="text-uppercase heading border-bottom mb-4">Резиденция Coatel Chereville</h2>

                <?php
                OwlCarouselWidget::begin([
                    'container' => 'div',
                    'containerOptions' => [
                        'id' => 'coatel_slider',
                        'class' => 'coatel_slider'
                    ],
                    'pluginOptions' => [
                        'autoplay' => true,
                        'autoplayTimeout' => 3000,
                        'items' => 1,
                        'loop' => true,
                        'itemsDesktop' => [1199, 3],
                        'itemsDesktopSmall' => [979, 3]
                    ]
                ]);
                ?>
                <div><img src="/media/hotels/coatel/coatel_01.jpg" alt="Image 1"></div>
                <div><img src="/media/hotels/coatel/coatel_02.jpg" alt="Image 2"></div>
                <div><img src="/media/hotels/coatel/coatel_03.jpg" alt="Image 3"></div>
                <div><img src="/media/hotels/coatel/coatel_04.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_05.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_06.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_07.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_08.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_09.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_10.jpg" alt="Image 4"></div>
                <div><img src="/media/hotels/coatel/coatel_11.jpg" alt="Image 4"></div>

                <?php OwlCarouselWidget::end(); ?>

                <div class="mb-0 lead text-justify">
                    <p>Coatel Chereville – резиденция, где можно ощутить комфорт и спокойствие благодаря современной
                        кухне и обслуживанию на высшем уровне. Главное кредо Coatel Chereville - возможность
                        долгосрочного, а также краткосрочного проживания по разумной цене.</p>
                    <p class="text-dark">Адрес : 1330, Сочо-донг, Сочо-гу, Сеул, Южная Корея</p>
                </div>
            </div>

        </div>
    </div>
</section>
