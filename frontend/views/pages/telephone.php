<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Телефон в аренду';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="section stretch-section" style="padding: 7em 0 1em 0">
    <div class="container">
        <div class="row justify-content-center mb-5 element-animate">
            <div class="col-md-8 text-center mb-5">
                <h1 class="text-uppercase heading border-bottom mb-4">Услуга "телефон в аренду"</h1>

                <div class="mb-0 lead text-justify">
                    <p>Для вашего удобства мы предоставляем телефон в аренду на все время проживания в Корее. Для
                        использования местной связи вам необходимо всего лишь внести оплату в размере 10 $ и свободно
                        использовать телефон для звонков по Южной Корее.</p>
                </div>

                <img src="/media/telephone/phone.jpg" alt="phone" style="width: 100%">
            </div>
        </div>
    </div>
</section>
