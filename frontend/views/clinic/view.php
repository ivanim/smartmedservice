<?php
/* @var $this yii\web\View */
/* @var $clinic \frontend\models\Clinics */
?>
<div class="row">
    <div class="col-md-2 col-md-offset-1">

    </div>
    <div class="col-md-10 col-md-offset-2">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <img class="clinic_view_img" src="<?= $clinic->imgUrl ?>" alt="<?= $clinic->name ?>">
            </div>
        </div>
        <h1 class="clinic_tite"><?= $clinic->name ?></h1>

        <p>
            <?= $clinic->description ?>
        </p>
    </div>
</div>

<style>
    .clinic_view_img{
        width: 60%;
        text-align: center;
    }
    .clinic_tite{
        text-align: center;
    }
</style>
