<?php

?>

<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('img/slider-2.jpg');">

        <div class="container">
            <div class="row slider-text align-items-center">
                <div class="col-md-7 col-sm-12 element-animate">
                    <h1>Лечение в Корее</h1>
                    <p>Мы предлагаем полный спектр туристических медицинских услуг в Корее</p>
                </div>
            </div>
        </div>

    </div>

    <div class="slider-item" style="background-image: url('img/slider-1.jpg');">
        <div class="container">
            <div class="row slider-text align-items-center">
                <div class="col-md-7 col-sm-12 element-animate">
                    <!--<h1>We Provide Health Care Solutions</h1>-->
                    <p>Наша компания сделает Ваше лечение в лучших больницах Кореи максимально комфортным.</p>
                </div>
            </div>
        </div>

    </div>

    <div class="slider-item" style="background-image: url('img/slider-3.jpg');">
        <div class="container">
            <div class="row slider-text align-items-center">
                <div class="col-md-7 col-sm-12 element-animate">
                    <!--<h1>We Provide Health Care Solutions</h1>-->
                    <p>
                        Мы гордимся тем уровнем сервиса, который мы достигли за последние годы. Ваше пребывание в Корее
                        наша забота. Мы предоставляем полный спектр услуг от встречи в аэропорту до получения ваших
                        медицинских документов после Вашего отбытия домой
                    </p>
                </div>
            </div>
        </div>

    </div>

</section>
