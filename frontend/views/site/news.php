<?php

?>

<section class="home-slider inner-page owl-carousel">
    <div class="slider-item" style="background-image: url('img/slider-2.jpg');">

        <div class="container">
            <div class="row slider-text align-items-center">
                <div class="col-md-7 col-sm-12 element-animate">
                    <h1>Новости компании</h1>
                    <p>Наши новости</p>
                </div>
            </div>
        </div>

    </div>

</section>
<!-- END slider -->


<section class="section bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_1.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_2.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_3.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_1.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_2.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 element-animate">
                <div class="media d-block media-custom text-left">
                    <img src="img/img_thumb_3.jpg" alt="Image Placeholder" class="img-fluid">
                    <div class="media-body">
                        <span class="meta-post">December 2, 2017</span>
                        <h3 class="mt-0 text-black"><a href="#" class="text-black">Lorem ipsum dolor sit amet elit</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p class="clearfix">
                            <a href="#" class="float-left">Read more</a>
                            <a href="#" class="float-right meta-chat"><span class="ion-chatbubble"></span> 8</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row element-animate">
            <div class="col-md-5">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
