<?php

namespace frontend\controllers;

use Yii;
use backend\models\Clinics;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClinicController implements the CRUD actions for Clinics model.
 */
class PagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionHotels()
    {
        return $this->render('hotels');
    }

    public function actionMedicalCoordinator()
    {
        return $this->render('medical-coordinator');
    }

    public function actionAirport()
    {
        return $this->render('airport');
    }

    public function actionTelephone()
    {
        return $this->render('telephone');
    }

    public function actionTourism()
    {
        return $this->render('tourism');
    }

}
