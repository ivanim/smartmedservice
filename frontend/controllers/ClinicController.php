<?php

namespace frontend\controllers;

use frontend\models\Clinics;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ClinicController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionView($slug)
    {
        $clinic = Clinics::findOne(['slug' => $slug]);

        return $this->render('view', [
            'clinic' => $clinic,
        ]);
    }

}
