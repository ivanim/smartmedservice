<?php

namespace frontend\models;

use common\behaviors\FileUploadBehavior;
use Yii;

/**
 * This is the model class for table "clinics".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $img
 * @property string $slug
 * @property string $teaserText
 */
class Clinics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clinics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'img', 'slug'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            'logo_upload' => [
                'class' => FileUploadBehavior::className(),
                'fileAttribute' => 'img',
                'saveDir' => '/web/media/clinic/',
                'fileName' => 'slug',
                'basePath' => Yii::getAlias('@frontend'),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'img' => 'Img',
        ];
    }

    public function getTeaserText($length = 50)
    {
        if (mb_strlen($this->description) <= $length){
            return $this->description;
        }
        return mb_substr($this->description, 0, $length) . '...';
    }

    public function getImgUrl()
    {
        return '/media/clinic/' . $this->img;
    }
}
