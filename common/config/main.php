<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'Asia/Almaty',
    'language' => 'ru-RU',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enableStrictParsing' => false,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                'crm/leads' => 'crm/lead',
                'crm/source' => 'crm/lead-source',
                'crm/status' => 'crm/lead-status',
                'leads/' => 'leads/lead',
                'users/' => 'user/index',
                'briefs/' => 'briefs/index',
                'admin/' => 'admin',
                //'<module>' => '<module>/default',
                //'<action>' => 'site/<action>',
                //'<controller>/<action>' => '<controller>/<action>',
                //'<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                //                'articles/<id:\d+>' => 'articles/default/single-article',
                //                'news/<id:\d+>' => 'news/default/single-news',
                //                'articles/<slug:[\w-]+>' => 'articles/default/index',
                //                'news/<slug:[\w-]+>' => 'news/default/index',
                //                'articles' => 'articles/default/preview',
                //'<controller>/<action>' => 'site/<controller>/<action>',
            ],
        ],
    ],
];
