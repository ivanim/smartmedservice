<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 13-Mar-18
 * Time: 11:16
 */

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * Class FileUploadBehavior
 * @package common\behaviors
 *
 * @property Model owner
 */
class FileUploadBehavior extends Behavior
{
    public $fileAttribute = 'file';
    public $saveDir = '/../frontend/web/upload/';
    public $fileNamePrefix = '';
    public $fileName = '';
    public $oldName;
    public $basePath = '';

    public function initVar()
    {
        $this->oldName = $this->owner->{$this->fileAttribute};
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'addFile',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteFile',
            ActiveRecord::EVENT_AFTER_FIND => 'initVar',
        ];
    }

    public function addFile()
    {
        $file = UploadedFile::getInstance($this->owner, $this->fileAttribute);
        if ($file) {
            if ($this->oldName && !$this->deleteFile()){
                return;
            }

            $fileName = $this->fileName ? $this->owner->{$this->fileName} : Yii::$app->security->generateRandomString();
            $uniqName = $this->fileNamePrefix . $fileName . '.' . $file->getExtension();

            $saveDirPath = $this->basePath . $this->saveDir;

            //VarDumper::dump($saveDirPath);die;

            if (!file_exists($saveDirPath) and !is_dir($saveDirPath)) {
                mkdir($saveDirPath, 0755, true);
            }

            if ($file->saveAs($saveDirPath . $uniqName)) {
                $this->owner->{$this->fileAttribute} = $uniqName;
            }
        }
        else{
            $this->owner->{$this->fileAttribute} = $this->oldName;
        }
    }

    public function deleteFile()
    {
        if ($this->oldName and file_exists($this->basePath . $this->saveDir . $this->owner->{$this->fileAttribute})) {
            //var_dump($this->owner);die;
            return unlink($this->basePath . $this->saveDir . $this->oldName);
            //return unlink($this->oldName);
        }
        return false;
    }

    public function getFile()
    {
        return $this->owner->{$this->fileAttribute};
    }

}