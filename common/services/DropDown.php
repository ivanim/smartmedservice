<?php
/**
 * @package Chocolife.me
 * @author  Moldabayev Vadim <moldabayev.v@chocolife.kz>
 */

namespace common\services;


use backend\models\Countries;
use yii\helpers\ArrayHelper;

class DropDown
{
    public static function countries()
    {
        $countries = Countries::find()->asArray()->all();

        return ArrayHelper::map($countries, 'id', 'name_ru');
    }

}