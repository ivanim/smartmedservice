<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\crm\models\LeadStatus */

$this->title = 'Create Lead Status';
$this->params['breadcrumbs'][] = ['label' => 'Lead Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
