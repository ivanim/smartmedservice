<?php

use yii\bootstrap\Progress;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\crm\models\Lead */
/* @var $event backend\modules\crm\models\LeadHistory */
/* @var $events array */
/* @var $status_bar array */

$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary fixed-w-h">
                <?php $status_form = ActiveForm::begin(); ?>

                <div class="box-header bg-info">Обновить статус</div>
                <div class="box-body">
                    <?= $status_form->field($model, 'status')
                        ->dropDownList(\yii\helpers\ArrayHelper::map(\backend\modules\crm\models\LeadStatus::find()
                            ->all(), 'id', 'name'),
                            [
                                'prompt'  => 'Select status',
                                'id'      => 'status',
                                'options' => [$model->status => ['selected' => false]],
                            ])
                        ->label(false) ?>

                    <?= $status_form->field($model, 'event_comment')->textarea(['id' => 'comment', 'rows' => 4])
                        ->label(false) ?>

                </div>
                <div class="box-footer">
                    <?= Html::submitButton('Добавить', ['id' => 'statBtn', 'class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>


        <div class="col-md-8">
            <!--<div class="box box-primary">
                <?php /*$status_form = ActiveForm::begin(); */ ?>

                <div class="box-header bg-info"></div>
                <div class="box-body">

                </div>
                <div class="box-footer">

                </div>

                <?php /*ActiveForm::end(); */ ?>
            </div>-->
            <div class="box box-primary fixed-w-h">
                <div class="box-header bg-info">История событий</div>

                <div class="box-body block-history">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th>Обновлен</th>
                            <th>Комментарий</th>
                        </tr>

                        <?php foreach ($events as $e): ?>

                            <tr>
                                <td><?= $e->date ?></td>
                                <td><?= $e->leadStatus->name ?></td>
                                <td><?= $e->updatedBy ? $e->updatedBy->username : null ?></td>
                                <td><?= $e->comment ?></td>
                            </tr>

                        <?php endforeach; ?>

                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary fixed-w-h2">

                <div class="box-header bg-info">Информация о лиде</div>
                <div class="box-body">
                    <table class="table ">
                        <tr>
                            <td>Дата создания</td>
                            <td><?= date('d-M-Y H:i', $model->created_at) ?></td>
                        </tr>
                        <tr>
                            <td>Текущий статус</td>
                            <td><?= $model->status_->name ?></td>
                        </tr>
                        <tr>
                            <td>Источник</td>
                            <td><?= $model->source_->name ?></td>
                        </tr>
                    </table>
                </div>
                <div class="box-footer">
                    <?= Progress::widget(['bars' => $status_bar]); ?>

                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Конвертировать лид <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href=<?= Url::toRoute([
                                    'lead/convert-lead',
                                    'id'     => $model->id,
                                    'target' => 'contact',
                                ]) ?>>Конвертировать в контакт</a></li>
                            <li><a href=<?= Url::toRoute([
                                    'lead/convert-lead',
                                    'id'     => $model->id,
                                    'target' => 'company',
                                ]) ?>>Конвертировать в компанию</a></li>
                        </ul>
                        <?= Html::a('Некачественный лид',
                            Url::toRoute(['lead/convert-lead', 'id' => $model->id, 'target' => 'junk']),
                            ['class' => 'btn btn-danger']) ?>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="box box-primary">
                <?php $data_form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                <?php $data_form->action = \yii\helpers\Url::toRoute(['lead/update', 'id' => $model->id]); ?>
                <div class="box-header bg-info">Данные</div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 ">
                            <?= $data_form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'company')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'description')->textarea(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $data_form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                            <?= $data_form->field($model, 'city')
                                ->dropDownList(\yii\helpers\ArrayHelper::map(\backend\modules\crm\models\City::find()
                                    ->all(), 'id', 'name'),
                                    ['prompt' => 'Select city']
                                ) ?>
                            <?= $data_form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.onready = function () {
        $('#no/*statBtn*/').on('click', function () {
            $.ajax({
                method: 'post',
                url: <?= \yii\helpers\Url::toRoute(['leads/lead/update'])?>,
                data: JSON.stringify([
                    {
                        Lead: {
                            status: $('#status').val()
                        }
                    },
                    {
                        comment: $('#comment').val()
                    }
                ]),
                success: function (data) {
                    if (data = 'ok') {
                        location.reload();
                    }
                }
            })
        })
    }

</script>

<style>
    .z-top {
        z-index: 999;
    }

    .page-header {
        height: 30px;
        margin: 5px 0 5px 0;
        padding: 5px;
    }

    .box {
    / / border: 1 px solid #ccc;
    / / padding: 5 px;
        overflow-y: hidden;
    }

    .fixed-w-h {
        min-height: 290px;
        max-height: 290px;
    }

    .fixed-w-h2 {
        min-height: 420px;
        max-height: 420px;
    }

    th:first-child, td:first-child {
        width: 150px;
    }

    .block-history {
        overflow-y: scroll;
        max-height: inherit;
    }

    .info {
    / / min-height: 280 px;
    / / background: #ddd;
    }

    .block-data input, .block-data select {
    / / height: 30 px;
    / / width: 100 %;
    }

    .block-data textarea {
    / / width: 100 %;
    }

    .block-data {
    / / min-height: auto;
    }

    .form-group {
    / / margin-bottom: 0;
    }

    .row {
    / / margin: 0 0 15 px 0;
    }

    .btn {
        border-radius: 0;
    }
</style>