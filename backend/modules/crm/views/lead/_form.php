<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\crm\models\Lead */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lead-form">

   <div class="row">
       <div class="col-md-6">
           <?php $form = ActiveForm::begin(); ?>

           <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'source')
               ->dropDownList(\yii\helpers\ArrayHelper::map(\backend\modules\crm\models\LeadSource::find()->all(), 'id', 'name'),
                   ['prompt'=>'Select source']
               )?>

           <?= $form->field($model, 'status')
               ->dropDownList(\yii\helpers\ArrayHelper::map(\backend\modules\crm\models\LeadStatus::find()->all(), 'id', 'name'),
                   ['prompt'=>'Select status']
               )?>

           <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
       </div>

       <div class="col-md-6">
           <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'city')
               ->dropDownList(\yii\helpers\ArrayHelper::map(\backend\modules\crm\models\City::find()->all(), 'id', 'name'),
               ['prompt'=>'Select city']
           )?>

           <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
       </div>
   </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>