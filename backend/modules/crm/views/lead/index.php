<?php

use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lead', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => CheckboxColumn::className()],
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'company',
            //'title',
            //'phone',
            'mobile',
            //'email:email',
            //'city',
            // 'address',
            'source_.name',
            'status_.name',
            'description',

            //['class' => 'yii\grid\ActionColumn'],
        ],

        'rowOptions' => function($model, $key, $index, $grid){
            if($model->status == 1){
                $color = '#e6e8ff';
            }
            elseif ($model->status == 6){
                $color = '#d6ffb1';
            }
            elseif ($model->status == 5){
                $color = '#ffcccb';
            }
            else{
                $color = '#f9f9f9';
            }
            return [
                'style' => 'background:' . $color,
                'onMouseOver'=>'this.style.cursor = "pointer"',
                'onclick'=>'window.location.href="' . \yii\helpers\Url::toRoute(['view', 'id'=>$model->id]) .'"'
            ];
        },
    ]); ?>
</div>
