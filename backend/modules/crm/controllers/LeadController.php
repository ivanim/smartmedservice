<?php

namespace backend\modules\crm\controllers;

use backend\modules\crm\models\Company;
use backend\modules\crm\models\Contact;
use backend\modules\crm\models\LeadHistory;
use backend\modules\crm\models\RefLeadConvert;
use common\models\User;
use Yii;
use backend\modules\crm\models\Lead;
use yii\data\ActiveDataProvider;
use yii\db\AfterSaveEvent;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LeadController implements the CRUD actions for Lead model.
 */
class LeadController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lead models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Lead::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lead model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->isPost && $model->isConverted()){
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (Yii::$app->request->post(StringHelper::basename(Lead::className()))) {
            $model->event_comment = $this->getFormAtt('event_comment');
            if ($model->changeStatus($this->getFormAtt('status'))) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('view', [
            'model' => $model,
            'events' => LeadHistory::find()->where(['lead_id' => $id])->all(),
            'status_bar' => $model->statusBarData()
        ]);
    }

    /**
     * @param string $attr
     * @return mixed
     */
    private function getFormAtt($attr)
    {
       return Yii::$app->request->post()[StringHelper::basename(Lead::className())][$attr];
    }

    /**
     * Creates a new Lead model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lead();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Lead model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->isConverted()){
            Yii::$app->session->setFlash('error', Yii::t('lead', 'Error. Lead converted'));
        }

        else  {
            if ($model->load(Yii::$app->request->post()))
            $model->save();
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Lead model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lead model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lead the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lead::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function addLead($data)
    {
        $model = new Lead();
        $model->attributes = $data;
        return $model->save(false);
    }


    /**
     * @param integer $id
     * @param string $target 'contact' or 'company'
     * @return \yii\web\Response
     */
    public function actionConvertLead($id, $target)
    {
        $lead = $this->findModel($id);
        if ($lead && !$lead->isConverted()){
            if ($target == 'contact'){
                $model = new Contact();
                $service = 1;
            }
            elseif ($target == 'company'){
                $model = new Company();
                $service = 2;
            }
            elseif ($target == 'junk'){
                $lead->changeStatus(Lead::STATUS_JUNK);
                return $this->redirect(['view', 'id' => $lead->id]);
            }
            else{
                Yii::$app->session->setFlash('error', Yii::t('lead', 'Error. Not converted'));
                return $this->redirect(['view', 'id' => $lead->id]);
            }

            $ref_lead_contr = new RefLeadConvert();

            if($model->convertFrom($lead) && $ref_lead_contr->saveRecord($id, $model->id, $service)){
                $lead->changeStatus(Lead::STATUS_CONVERTED);
                Yii::$app->session->setFlash('success', Yii::t('lead', 'Converted'));
            }

            /*$lead->convertTo($target) ?
                Yii::$app->session->setFlash('success', Yii::t('lead', 'Converted'))
                : Yii::$app->session->setFlash('error', Yii::t('lead', 'Error. Not converted'));*/
        }

        return $this->redirect(['view', 'id' => $lead->id]);
    }

}
