<?php

namespace backend\modules\crm\models;

use Yii;

/**
 * This is the model class for table "ref_lead_convert".
 *
 * @property integer $id
 * @property integer $lead_id
 * @property integer $convert_id
 * @property integer $type
 */
class RefLeadConvert extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_lead_convert';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_id', 'convert_id', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lead_id' => Yii::t('app', 'Lead ID'),
            'convert_id' => Yii::t('app', 'Convert ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    public function saveRecord($lead_id, $convert_id, $type)
    {
        $this->lead_id = $lead_id;
        $this->convert_id = $convert_id;
        $this->type = $type;
        return $this->save();
    }
}
