<?php

namespace backend\modules\crm\models;

use Yii;

/**
 * This is the model class for table "con_contact".
 *
 * @property integer $id
 * @property string $f_name
 * @property string $s_name
 * @property string $m_name
 * @property string $title
 * @property integer $company_id
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property integer $city
 * @property string $address
 * @property integer $status
 * @property string $description
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_name', 'mobile'], 'required'],
            [[/*'company_id',*/ 'city', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['f_name', 's_name', 'm_name', 'title', 'phone', 'mobile', 'email', 'address', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'f_name' => Yii::t('app', 'F Name'),
            's_name' => Yii::t('app', 'S Name'),
            'm_name' => Yii::t('app', 'M Name'),
            'title' => Yii::t('app', 'Title'),
            //'company_id' => Yii::t('app', 'Company ID'),
            'phone' => Yii::t('app', 'Phone'),
            'mobile' => Yii::t('app', 'Mobile'),
            'email' => Yii::t('app', 'Email'),
            'city' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'status' => Yii::t('app', 'Status'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactQuery(get_called_class());
    }

    /**
     * @param Lead $lead
     * @return bool
     */
    public function convertFrom($lead)
    {
        $this->f_name = $lead->name;
        $this->title = $lead->title;
        $this->phone = $lead->phone;
        $this->mobile = $lead->mobile;
        $this->email = $lead->email;
        $this->city = $lead->city;
        $this->address = $lead->address;
        $this->address = $lead->address;
        $this->description = 'from lead #' . $lead->id;
        if ($lead->company && ($company = Company::find()->where(['name' => $lead->company])->one())){
            $this->company_id = $company->id;
        }

        return $this->save();
    }
}
