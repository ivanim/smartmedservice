<?php

namespace backend\modules\crm\models;

use Yii;

/**
 * This is the model class for table "ref_city".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Lead[] $ledDatas
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLedDatas()
    {
        return $this->hasMany(Lead::className(), ['city' => 'id']);
    }
}
