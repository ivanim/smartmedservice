<?php

namespace backend\modules\crm\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "leads".
 *
 * @property integer $id
 * @property string $name
 * @property string $company
 * @property string $title
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property integer $city
 * @property string $address
 * @property integer $source
 * @property integer $status
 * @property string $description
 * @property string $message
 * @property int $created_at [int(11)]
 * @property int $created_by [int(11)]
 * @property int $updated_at [int(11)]
 * @property int $updated_by [int(11)]
 *
 * @property City $city_
 * @property LeadSource $source_
 * @property LeadStatus $status_
 */
class Lead extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_JUNK = 5;
    const STATUS_CONVERTED = 6;

    const SOURCE_APPOINTMENT = 1;
    /**
     * @var
     */
    public $event_comment;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leads';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                //'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source', 'status'], 'required'],
            [['city', 'source', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'company', 'title', 'phone', 'mobile', 'email', 'address', 'description'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 1024],
            [['city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city' => 'id']],
            [['source'], 'exist', 'skipOnError' => true, 'targetClass' => LeadSource::className(), 'targetAttribute' => ['source' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => LeadStatus::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'company' => 'Company',
            'title' => 'Title',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'city' => 'City',
            'address' => 'Address',
            'source' => 'Source',
            'status' => 'Status',
            'description' => 'Description',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity_()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource_()
    {
        return $this->hasOne(LeadSource::className(), ['id' => 'source']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus_()
    {
        return $this->hasOne(LeadStatus::className(), ['id' => 'status']);
    }

    public function statusBarData()
    {
        $data = [];
        $del_width = 0.5;
        $count = LeadStatus::find()->count();

        $count ? null : $count = 1;
        $percent = (100 - ($count - 1) * $del_width) / $count;
        for ($i = 1; $i <= $count; $i++) {
            $item['percent'] = $percent;
            $color = $i <= $this->status && $this->status != self::STATUS_JUNK ? 'success' : 'danger';
            $item['options'] = ['class' => 'progress-bar-' . $color];
            if ($i == $this->status) {
                $item['options']['data-toggle'] = 'tooltip';
                $item['options']['data-placement'] = 'bottom';
                $item['options']['data-title'] = $this->status_->name;
            }
            $delimeter['percent'] = $del_width;
            $delimeter['options'] = ['style' => 'background-color:#ccc; width:' . $del_width . '%'];
            $data[] = $item;
            $i != $count ? $data[] = $delimeter : null;
        }
        return $data;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->isNewRecord) {
            $this->on(Lead::EVENT_AFTER_INSERT, [LeadHistory::className(), 'saveEvent']);
        }

        if (!$this->status) {
            $this->status = self::STATUS_NEW;
        }
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @param $status integer
     * @return bool
     */
    public function changeStatus($status)
    {
        if (!$this->isStatusAllowed($status)) {
            return false;
        }
        $this->on(Lead::EVENT_AFTER_UPDATE, [LeadHistory::className(), 'saveEvent']);
        $this->status = $status;
        return $this->save();
    }

    /**
     * @param $status integer
     * @return bool
     */
    private function isStatusAllowed($status)
    {
        return $status >= $this->status;
    }


/*    public function convertTo($target = 'contact')
    {
        if ($target === 'contact') {
            $model = new Contact();
            $model->on(Contact::EVENT_AFTER_INSERT, function () {
                $this->changeStatus(self::STATUS_CONVERTED);
            });
        } elseif ($target === 'company') {
            $model = new Company();

        } elseif ($target === 'junk') {
            return $this->changeStatus(self::STATUS_JUNK);
        } else {
            return false;
        }

        return $model->convertFrom($this);
    }*/

    public function isConverted()
    {
        return ($this->status == self::STATUS_CONVERTED || $this->status == self::STATUS_JUNK);
    }

    public function loadFromMain(array $data)
    {
        $this->name = $data['appointment_name'];
        $this->mobile = $data['appointment_phone'];
        $this->email = $data['appointment_email'];
        $this->message = $data['appointment_message'];
        $this->status = self::STATUS_NEW;
        $this->source = self::SOURCE_APPOINTMENT;
    }
}
