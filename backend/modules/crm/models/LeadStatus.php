<?php

namespace backend\modules\crm\models;

use Yii;

/**
 * This is the model class for table "ref_lead_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $code
 *
 * @property Lead[] $ledDatas
 */
class LeadStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_lead_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Status',
            'description' => 'Description',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLedDatas()
    {
        return $this->hasMany(Lead::className(), ['status' => 'id']);
    }
}
