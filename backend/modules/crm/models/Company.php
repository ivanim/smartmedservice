<?php

namespace backend\modules\crm\models;

use Yii;

/**
 * This is the model class for table "com_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $BIN
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['BIN'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'BIN' => Yii::t('app', 'Bin'),
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * @param $lead Lead
     * @return bool
     */
    public function convertFrom($lead)
    {
        $this->name = $lead->company;
        return $this->save();
    }
}
