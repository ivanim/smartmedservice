<?php

namespace backend\modules\crm\models;

use common\models\User;
use Yii;
use yii\base\Event;

/**
 * This is the model class for table "lead_history".
 *
 * @property integer $id
 * @property integer $lead_id
 * @property string $date
 * @property integer $lead_status
 * @property integer $updated_by
 * @property string $comment
 *
 * @property Lead $lead
 * @property LeadStatus $leadStatus
 * @property LeadStatus $status
 * @property User $updatedBy
 */
class LeadHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_id', 'date', 'lead_status'], 'required'],
            [['lead_id', 'lead_status', 'updated_by'], 'integer'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['lead_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['lead_id' => 'id']],
            [['lead_status'], 'exist', 'skipOnError' => true, 'targetClass' => LeadStatus::className(), 'targetAttribute' => ['lead_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lead_id' => Yii::t('app', 'Lead ID'),
            'date' => Yii::t('app', 'Date'),
            'lead_status' => Yii::t('app', 'Lead Status'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Lead::className(), ['id' => 'lead_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadStatus()
    {
        return $this->hasOne(LeadStatus::className(), ['id' => 'lead_status']);
    }

    /**
     * @param $event Event
     */
    public static function saveEvent($event)
    {
        $lead_event = new self();
        $lead_event->lead_id = $event->sender->id;
        $lead_event->date = date('Y-m-d H:i');
        $lead_event->updated_by = $event->sender->updated_by;
        $lead_event->comment = $event->sender->event_comment;
        if (self::isStatusAllowed()){
            $lead_event->lead_status = $event->sender->status;
            $lead_event->save();
        }
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return bool
     */
    public static function isStatusAllowed()
    {
        return true;
    }
}
