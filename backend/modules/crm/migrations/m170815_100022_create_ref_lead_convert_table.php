<?php

namespace backend\modules\crm\migrations;

use console\components\db\SmsMigration;

/**
 * Handles the creation of table `ref_lead_convert`.
 */
class m170815_100022_create_ref_lead_convert_table extends SmsMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ref_lead_convert', [
            'id' => $this->primaryKey(),
            'lead_id' => $this->integer(),
            'convert_id' => $this->integer(),
            'type' => $this->integer(),
        ], $this->getTableOptions());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ref_lead_convert');
    }
}
