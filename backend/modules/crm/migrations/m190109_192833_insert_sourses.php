<?php

namespace backend\modules\crm\migrations;

use yii\db\Migration;

class m190109_192833_insert_sourses extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->getSources() as $source){
            $this->insert(
                'ref_lead_source',
                [
                    'name' => $source['name'],
                    'code' => $source['code'],
                    'description' => $source['description']
                ]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo 'No revert';
    }

    private function getSources()
    {
        return [
            [
                'name' => 'Форма',
                'code' => 'form',
                'description' => 'С формы на главной странице'
            ],
            [
                'name' => 'Телефон',
                'code' => 'phone',
                'description' => 'С телефонного звонка'
            ],
            [
                'name' => 'Инстаграм',
                'code' => 'instagram',
                'description' => 'Инстаграм'
            ]
        ];
    }
}
