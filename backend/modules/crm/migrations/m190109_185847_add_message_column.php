<?php

namespace backend\modules\crm\migrations;

use yii\db\Migration;

class m190109_185847_add_message_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            'leads',
            'message',
            $this->text()
                ->comment('Сообщение от пользователя')
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('leads', 'message');
    }
}
