<?php

namespace backend\modules\crm\migrations;

use yii\db\Migration;

class m190109_191932_insert_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->getStatuses() as $status){
            $this->insert(
                'ref_lead_status',
                [
                    'name' => $status['name'],
                    'code' => $status['code'],
                    'description' => $status['description']
                ]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo 'No revert';
    }

    private function getStatuses()
    {
        return [
            [
                'name' => 'Новый',
                'code' => 'new',
                'description' => 'Новый лид'
            ],
            [
                'name' => 'Не качественный',
                'code' => 'junk',
                'description' => 'Не качественный'
            ],
            [
                'name' => 'Конвертирован',
                'code' => 'converted',
                'description' => 'Конвертирован в контакт'
            ]
        ];
    }
}
