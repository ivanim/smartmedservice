<?php

namespace backend\modules\crm\migrations;

use console\components\db\SmsMigration;

class m170808_114620_create_tables_for_leads_module extends SmsMigration
{
    public function safeUp()
    {
        $this->createTable('leads',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'company' => $this->string(),
                'title' => $this->string(),
                'phone' => $this->string(20),
                'mobile' => $this->string(20),
                'email' => $this->string(),
                'city' => $this->integer(),
                'address' => $this->string(),
                'source' => $this->integer()->notNull(),
                'status' => $this->integer()->notNull(),
                'description' => $this->string(),
                'created_at' => $this->integer(),
                'created_by' => $this->integer(),
                'updated_at' => $this->integer(),
                'updated_by' => $this->integer(),
            ], $this->getTableOptions());

        $this->createTable('ref_city',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
            ], $this->getTableOptions());

        $this->createTable('ref_lead_source',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'description' => $this->string(),
                'code' => $this->string(32),
            ], $this->getTableOptions());

        $this->createTable('ref_lead_status',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'description' => $this->string(),
                'code' => $this->string(),
            ], $this->getTableOptions());

        $this->createTable('lead_history',
            [
                'id' => $this->primaryKey(),
                'lead_id' => $this->integer()->notNull(),
                'date'=>$this->dateTime()->notNull(),
                'lead_status'=>$this->integer()->notNull(),
                'updated_by'=>$this->integer(),
                'comment'=>$this->string()
            ], $this->getTableOptions());

        $this->addForeignKey(
            'fk_lead_city',
            'leads',
            'city',
            'ref_city',
            'id'
        );

        $this->addForeignKey(
            'fk_lead_source',
            'leads',
            'source',
            'ref_lead_source',
            'id'
        );

        $this->addForeignKey(
            'ref_lead_status',
            'leads',
            'status',
            'ref_lead_status',
            'id'
        );

        $this->addForeignKey(
            'fk_lead_hist_lead',
            'lead_history',
            'lead_id',
            'leads',
            'id'
        );

        $this->addForeignKey(
            'fk_lead_hist_lead_status',
            'lead_history',
            'lead_status',
            'ref_lead_status',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_lead_hist_lead_status', 'lead_history');
        $this->dropForeignKey('fk_lead_hist_lead', 'lead_history');
        $this->dropForeignKey('ref_lead_status', 'leads');
        $this->dropForeignKey('fk_lead_source', 'leads');
        $this->dropForeignKey('fk_lead_city', 'leads');
        $this->dropTable('lead_history');
        $this->dropTable('ref_lead_status');
        $this->dropTable('ref_lead_source');
        $this->dropTable('ref_city');
        $this->dropTable('leads');
    }

}
