<?php

namespace backend\modules\crm\migrations;

use console\components\db\SmsMigration;
/**
 * Handles the creation of table `contact`.
 */
class m170814_052810_create_contact_table extends SmsMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'f_name' => $this->string()->notNull(),
            's_name' => $this->string(),
            'm_name' => $this->string(),
            'title' => $this->string(),
            //'company_id' => $this->integer(),
            'phone' => $this->string(),
            'mobile' => $this->string()->notNull(),
            'email' => $this->string(),
            'city' => $this->integer(),
            'address' => $this->string(),
            'status' => $this->integer(),
            'description' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ],  $this->getTableOptions());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }
}
