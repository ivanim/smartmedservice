<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Страны');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="countries-index">

    <p>
        <?= Html::a(Yii::t('app', 'Добавить страну'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            'name_kz',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
