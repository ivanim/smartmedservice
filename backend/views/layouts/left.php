<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= ucfirst(Yii::$app->user->identity->username) ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],

                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users']],
                    ['label' => 'Анкеты', 'icon' => 'address-card', 'url' => ['/briefs']],


                    ['label' => 'CRM', 'icon' => 'address-card', 'url' => '#',
                         'items' => [
                             ['label' => 'Лиды', 'icon' => 'users', 'url' => ['/crm/leads/']],
                             //['label' => 'Города', 'icon' => 'building', 'url' => ['/crm/cities']],
                             ['label' => 'Источники', 'icon' => 'circle', 'url' => ['/crm/source']],
                             ['label' => 'Статусы', 'icon' => 'bullseye', 'url' => ['/crm/status']],
                         ]
                    ],


                    ['label' => 'Справочники', 'icon' => 'list-ul', 'url' => '#',
                        'items' => [
                            ['label' => 'Страны', 'icon' => 'globe', 'url' => ['/countries']],
                            ['label' => 'Города', 'icon' => 'building', 'url' => ['/cities']],
                        ]
                    ],

                    ['label' => 'Клиники', 'icon' => 'plus', 'url' => ['/clinic']],

                    /////////////////////////////////////////////////////////////////////////////////////////////
                    ['label' => '', 'options' => ['class' => 'header']],

                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'], 'visible' => YII_ENV_DEV],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'], 'visible' => YII_ENV_DEV],
                    ['label' => 'RBAC', 'url' => ['/admin'], 'visible' => isAdmin()],
                    /*[
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>

</aside>
