<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


//if (Yii::$app->controller->action->id === 'login' /*or
//    ((Yii::$app->controller->action->id === 'error') && Yii::$app->user->isGuest)*/) {
///**
// * Do not use this code in your template. Remove it.
// * Instead, use the code  $this->layout = '//main-login'; in your controller.
// */
//    echo $this->render(
//        'main-login',
//        ['content' => $content]
//    );
//} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>

    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?php if (!Yii::$app->user->isGuest): ?>
            <?= $this->render(
                'left.php',
                ['directoryAsset' => $directoryAsset]
            )
            ?>
        <?php endif; ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <style>
        .navbar-static-top, .logo{
            background-color: #395a6d !important;
        }
        .main-header .sidebar-toggle:before{
            content: '';
        }
        <?php if (Yii::$app->user->isGuest): ?>
             .content-wrapper, .main-footer{
                 margin-left: 0;
             }
             .login-box {
                 margin: 0 auto;
             }
        <?php endif; ?>
    </style>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
