<?php

/* @var $this yii\web\View */

$this->title = '';
?>
<div class="box box-primary">

    <div class="box-body">

        <div class="row">
            <div class="col-bg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Пользователи</h3>

                        <p>Всего: <?= \common\models\User::find()->count()?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-bg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <!--<h3>53<sup style="font-size: 20px">%</sup></h3>-->
                        <h3>Анкеты</h3>

                        <p>Анкеты пользователей</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="/briefs" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-bg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="small-box bg-yellow">
                    <div class="inner">

                        <h3>CRM</h3>

                        <p>&nbsp;</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="/crm" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
        </div>

    </div>
</div>
