<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clinics-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Добавить клинику', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'description',
                'label' => 'Описание',
                'contentOptions' => [
                    'style' => 'white-space: normal'
                ],
                'value' => function($model) {
                    return $model->getTeaserText(450);
                }
            ],
            'slug',

            [
                'attribute' => 'img',
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::img($model->imgUrl, ['style' => 'width: 100px']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
