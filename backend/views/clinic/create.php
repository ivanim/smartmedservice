<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Clinics */

$this->title = 'Добавить новую клинику';
$this->params['breadcrumbs'][] = ['label' => 'Clinics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clinics-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
