<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Анкеты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="briefs-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Новая анкета'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'country_id',
            'birth_date',
            'sex',
            'phone',
            //'illnesses:ntext',
            //'history:ntext',
            //'diagnosis:ntext',
            //'symptoms:ntext',
            //'recommendations:ntext',
            //'result:ntext',
            //'medications:ntext',
            //'purpose:ntext',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
