<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Briefs */

$this->title = Yii::t('app', 'Заполнить анкету');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Анкеты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="briefs-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
