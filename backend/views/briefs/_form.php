<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Briefs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'country_id')->dropDownList(\common\services\DropDown::countries()) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'illnesses')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'diagnosis')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'recommendations')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'medications')->textarea(['rows' => 6]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'birth_date')->textInput() ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'sex')->radioList([1 => 'МУЖ', 0 => 'ЖЕН'], []) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'phone')->textInput([
                                'maxlength'      => true,
                                'data-inputmask' => '&quot;mask&quot;: &quot;(999) 999-9999&quot;',
                                'data-mask'      => "",
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'history')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'symptoms')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'result')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'purpose')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-flat btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
