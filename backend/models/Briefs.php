<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "briefs".
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property string $birth_date
 * @property int $sex
 * @property string $phone
 * @property string $illnesses
 * @property string $history
 * @property string $diagnosis
 * @property string $symptoms
 * @property string $recommendations
 * @property string $result
 * @property string $medications
 * @property string $purpose
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Briefs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'briefs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'sex', 'created_by', 'updated_by'], 'integer'],
            [['birth_date', 'created_at', 'updated_at'], 'safe'],
            [['illnesses', 'history', 'diagnosis', 'symptoms', 'recommendations', 'result', 'medications', 'purpose'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'ФИО'),
            'country_id' => Yii::t('app', 'Страна'),
            'birth_date' => Yii::t('app', 'Дата рождения'),
            'sex' => Yii::t('app', 'Пол'),
            'phone' => Yii::t('app', 'Телефон'),
            'illnesses' => Yii::t('app', 'Наличие хронических заболеваний'),
            'history' => Yii::t('app', 'История болезни'),
            'diagnosis' => Yii::t('app', 'Диагноз'),
            'symptoms' => Yii::t('app', 'Симптомы'),
            'recommendations' => Yii::t('app', 'Рекомендации врачей (по месту жительства)'),
            'result' => Yii::t('app', 'Результаты обследований'),
            'medications' => Yii::t('app', 'Принимаемые лекарственные препараты'),
            'purpose' => Yii::t('app', 'Цель приезда'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
