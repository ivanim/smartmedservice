<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.12.18
 * Time: 0:02
 */

namespace backend\models;


class Clinics extends \frontend\models\Clinics
{
    public function getImgUrl()
    {
        return \Yii::$app->params['frontend-site'] . parent::getImgUrl();
    }
}