<?php
/**
 * Created by IntelliJ IDEA.
 * User: TOLK
 * Date: 24.07.2015
 * Time: 1:11
 */

/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use console\components\db\TolkMigration;

class <?= $className ?> extends TolkMigration
{
    private $tablename = '';
    private $tablecomment = '';

    public function up()
    {

        $this->createTable('{{%'.$this->tablename.'}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'user_id' => $this->integer()->notNull(),
            'company_id' => $this->integer(),

            //'code' => $this->string(20)->notNull().' COMMENT "Код"',

            'is_read' => $this->smallInteger(1).' COMMENT "Прочитано"',

            ], $this->getTableOptions($this->tablecomment));

/*$sql = <<<SQL

SQL;

        $this->execute($sql);
*/
    }

    public function down()
    {
        $this->dropTable('{{%'.$this->tablename.'}}');
        //echo "<?= $className ?> cannot be reverted.\n";
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}