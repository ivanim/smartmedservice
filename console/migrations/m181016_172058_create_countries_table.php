<?php

use yii\db\Migration;

/**
 * Handles the creation of table `countries`.
 */
class m181016_172058_create_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'name_ru' => $this->string(32),
            'name_kz' => $this->string(32),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('countries');
    }
}
