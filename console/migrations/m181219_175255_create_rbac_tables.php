<?php

use yii\db\Migration;

/**
 * Class m181219_175255_create_rbac_tables
 */
class m181219_175255_create_rbac_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $oldApp = \Yii::$app;
        new \yii\console\Application([
            'id'            => 'Command runner',
            'basePath'      => '@app',
            'components'    => [
                'db' => $oldApp->db,
                'authManager' => [
                    'class' => 'yii\rbac\DbManager',
                    'defaultRoles' => ['guest']
                ],
            ],
        ]);
        \Yii::$app->runAction('migrate',['migrationPath' => '@yii/rbac/migrations', 'interactive' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m181219_175255_create_rbac_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_175255_create_rbac_tables cannot be reverted.\n";

        return false;
    }
    */
}
