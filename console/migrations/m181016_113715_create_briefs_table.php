<?php

use yii\db\Migration;

/**
 * Handles the creation of table `briefs`.
 */
class m181016_113715_create_briefs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('briefs', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country_id' => $this->integer(),
            'birth_date' => $this->date(),
            'sex' => $this->boolean(),
            'phone' => $this->string(20),
            'illnesses' => $this->text(),
            'history' => $this->text(),
            'diagnosis' => $this->text(),
            'symptoms' => $this->text(),
            'recommendations' => $this->text(),
            'result' => $this->text(),
            'medications' => $this->text(),
            'purpose' => $this->text(),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('briefs');
    }
}
