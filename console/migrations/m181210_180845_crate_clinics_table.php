<?php

use console\components\db\SmsMigration;

class m181210_180845_crate_clinics_table extends SmsMigration
{
    private $tablename = 'clinics';
    private $tablecomment = 'Клиники';

    public function up()
    {
        $this->createTable('{{%'.$this->tablename.'}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'img' => $this->string(),
            'slug' => $this->string(32),
            ], $this->getTableOptions($this->tablecomment));

    }

    public function down()
    {
        $this->dropTable('{{%'.$this->tablename.'}}');
    }
}